/*
This program will Switch your SonOff Touch.
Thhe original code frame is from Tzapu I believe, 
but it has been altered and expanded.

*/
#include <FS.h>                   
#include <ESP8266WiFi.h>          
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>
#include "PubSubClient.h"
#include <ArduinoOTA.h>  

//GPIO Pin definitions
#define WiFiLED   13
#define TouchLED  12 
#define TouchInput 0
#define HOSTNAME "Sonoff-Touch-Study"

//Only output info to serial if this is HIGH
#define fDebug true

//Set our inital states and values
boolean ledState = false;
boolean lastButtonState = false;
boolean buttonState = false;
unsigned long lastDebounceTime = 0;  
unsigned long debounceDelay = 50;    // debounce time
int   signalStrength;

#define MQTT_VERSION    MQTT_VERSION_3_1_1
const PROGMEM uint16_t  MQTT_SERVER_PORT          = 1883;
const PROGMEM char*     MQTT_CLIENT_ID            = "Study Touch Switch2";
const PROGMEM char*     MQTT_USER                 = "";
const PROGMEM char*     MQTT_PASSWORD             = "";
const char*             MQTT_LIGHT_STATE_TOPIC    = "study/touch/status";
const char*             MQTT_LIGHT_COMMAND_TOPIC  = "study/touch/switch";
const char*             LIGHT_ON                  = "ON";
const char*             LIGHT_OFF                 = "OFF";
long starttime;         // determines the update period
long currenttime;       // ditto
String  IP;                   // IPaddress of ESP
String MAC;                //MAC address
char buff_msg[20];             // mqtt message
byte i;
String naam = (__FILE__);     // filenaam


//Create objects for MQTT and define IP Address class
WiFiClient espClient;
PubSubClient client(espClient);
IPAddress MQTT_SERVER_IP;

void setup() {
   //strip path van filenaam
  byte p1 = naam.lastIndexOf('\\');
  byte p2 = naam.lastIndexOf('.');
  naam = naam.substring(p1 + 1, p2);
  
  //-----------------

  if (fDebug) {
    Serial.begin(115200); 
  }
  //Setup GPIO pins for input/output as needed
  pinMode(WiFiLED, OUTPUT);
  pinMode(TouchLED, OUTPUT);
  pinMode(TouchInput, INPUT);
  digitalWrite(TouchLED, HIGH);
  delay(1000);

  //Turn LEDS's off :)
  digitalWrite(WiFiLED, HIGH);
  digitalWrite(TouchLED, ledState);
  const char *hostname = HOSTNAME;   //for OTA

  //Instantiate the WifManager library
  WiFiManager wifiManager;
  //Set a host name
  wifi_station_set_hostname("sonoffTouch");
  //Un remark this if your want to clear the saved connection settings.
  //wifiManager.resetSettings();
  if (fDebug) {
    wifiManager.setDebugOutput(true);
  }
  //If autoconnection fails, go into AP mode so the user can configure the WiFi connection to the local WLan
  if (!wifiManager.autoConnect("TouchMeAP")) {
    delay(3000);
    ESP.reset();
  } else {
    //WiFi LED is on when low
    digitalWrite(WiFiLED, LOW);
    //get MQTT servers address if it is on a known domain name, otherise just use known IP
    WiFi.hostByName("192.168.1.103", MQTT_SERVER_IP);
    if (fDebug) {
      Serial.print("MQTT_SERVER_IP: ");
      Serial.println(MQTT_SERVER_IP);
    }
    //Establish conenction to MQTT server
    client.setServer(MQTT_SERVER_IP , MQTT_SERVER_PORT);
    client.setCallback(callback);
    //Phew, have a bit of a breather to give everything a chance to settle down
    delay(1000);
    starttime = millis();
  }
  
  IP = WiFi.localIP().toString();
  MAC = WiFi.macAddress();
  //--------------------------

  //OTA
  ArduinoOTA.onStart([]() {
    Serial.println("Start OTA");
  });
  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
    else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
    else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
    else if (error == OTA_END_ERROR) Serial.println("End Failed");
  });
  ArduinoOTA.setHostname(hostname);
  ArduinoOTA.begin();

}
// function called to publish the state of the light (on/off)
void publishLightState() {
  if (fDebug) {
    Serial.println("PublishLightState Called");
    Serial.print("Light State: ");
    Serial.println(ledState);
  }
  if (ledState == HIGH) {
    if (fDebug) {
      Serial.print("Light state high ");
      Serial.println(ledState);
    }
    client.publish(MQTT_LIGHT_STATE_TOPIC, LIGHT_ON, true);
  } else {
    if (fDebug) {
      Serial.print("Light state low ");
      Serial.println(ledState);
    }
    client.publish(MQTT_LIGHT_STATE_TOPIC, LIGHT_OFF, true);
  }
}

// function called to turn on/off the light
void setLightState() {
  //Set our light state, ie turn the touch LED on/off which is the same GPIO pin as the relay.
  digitalWrite(TouchLED, ledState);
  //Feedback to HA our new state
  publishLightState();
}

// function called when a MQTT message arrived
void callback(char* p_topic, byte* p_payload, unsigned int p_length) {
  // concat the payload into a string
  if (fDebug) {
    Serial.println("Callback...");
  }
  String payload;
  for (uint8_t i = 0; i < p_length; i++) {
    payload.concat((char)p_payload[i]);
  }
  if (fDebug) {
    Serial.print("Payload: ");
    Serial.println(payload);
  }
  // handle message topic
  if (String(MQTT_LIGHT_COMMAND_TOPIC).equals(p_topic)) {
    // test if the payload is equal to "ON" or "OFF"
    if (payload.equals(String(LIGHT_ON))) {
      if (ledState != true) {
        ledState = true;
        setLightState();
        publishLightState();
      }
    } else if (payload.equals(String(LIGHT_OFF))) {
      if (fDebug) {
        Serial.println("Light Off");
        Serial.println("Light turned off");
      }
      ledState = false;
      setLightState();
      publishLightState();
    }
  }
}
void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    if (fDebug) {
      Serial.print("INFO: Attempting MQTT connection...");
    }
    // Attempt to connect
    if (client.connect(MQTT_CLIENT_ID, MQTT_USER, MQTT_PASSWORD)) {
      if (fDebug) {
        Serial.println("INFO: connected");
      }
      // Once connected, publish an announcement...
      publishLightState();
      // ... and resubscribe
      client.subscribe(MQTT_LIGHT_COMMAND_TOPIC);
    } else {
      if (fDebug) {
        Serial.print("ERROR: failed, rc=");
        Serial.print(client.state());
        Serial.println("DEBUG: try again in 5 seconds");
      }
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void loop() {
   //ota loop
  ArduinoOTA.handle();

  if (!client.connected()) {
    if (fDebug) {
      Serial.println("Reconnect");
    }
    reconnect();
  }
  //Get the current button state
  int reading = digitalRead(TouchInput);
  //De-bounce the button
  if (reading != lastButtonState) {
    lastDebounceTime = millis();
  }
  if ((millis() - lastDebounceTime) > debounceDelay) {
    // whatever the reading is at, it's been there for longer
    // than the debounce delay, so take it as the actual current state:

    // if the button state has changed:
    if (reading != buttonState) {
      buttonState = reading;

      // only toggle the LED if the new button state is HIGH
      if (buttonState == HIGH) {
        ledState = !ledState;
      }
      // set the LED:
      setLightState();
    }
  }
  //--------------periodic update
  currenttime = millis();
  if (currenttime - starttime > 30000)
  {
    publishLightState();
    Serial.print("interval update");
    starttime = millis();
    //----
    for (i = 0; i < 16; i++) {
      buff_msg[i] = IP[i];
    }
    buff_msg[i] = '\0';
    client.publish("study/touch/IP", buff_msg );
    //rssi
    signalStrength = WiFi.RSSI();
    sprintf(buff_msg, "%d", signalStrength);
    client.publish("study/touch/RSSI", buff_msg );
    //-mac
    for (i = 0; i < 17; i++) {
      buff_msg[i] = MAC[i];
    }
    buff_msg[i] = '\0';
    client.publish("study/touch/MAC", buff_msg);
  
  for (i=0; i<17; i++) {
    buff_msg[i]=naam[i];
  }
  buff_msg[i]= '\0';
  client.publish("study/touch/naam", naam.c_str());
  }
  //-----------------------------
  // save the reading.  Next time through the loop,
  // it'll be the lastButtonState:
  lastButtonState = reading;
  delay(64);
  client.loop();
}